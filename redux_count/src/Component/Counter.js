import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { decrement, increment,resetVal } from './../Redux/actions/index';

const Counter = () => {
const [number, setnumber] = useState(1)
    const count = useSelector(state => state.counter)
    const dispatch = useDispatch()

    return (
        <>
            <h1 style={{ fontSize: '50px' ,textAlign:'center'}}>{count}</h1>
           
            <div style={{ fontSize: '50px' ,textAlign:'center',alignItems:'center'}}>
                <h4>choose number to increment/decrement by value</h4>
            <input type='number'
             onChange={e => setnumber(parseInt(e.target.value))} 
             value={number}
             /><br />
            <button onClick={() => dispatch(decrement(number))}> <h1> SUB</h1></button>
            <button onClick={() => dispatch(resetVal())}> <h1> RESET</h1></button>
            <button onClick={() => dispatch(increment(number))}> <h1> ADD </h1></button>
            </div>
        </>
    )
}

export default Counter
