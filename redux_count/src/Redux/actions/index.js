export const increment = (val) => {
    return {
        type: "INCREMENT",
        add: val
    }
}

export const decrement = (val) => {
    return {
        type: "DECREMENT",
        sub: val
    }
}

export const resetVal = (val) => {
    return {
        type: "RESET",
        payload: val
    }
}
