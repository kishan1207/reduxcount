const CounterReducer = (state = 0, action) => {
    switch (action.type) {
        case "INCREMENT": return state + action.add;
        case "DECREMENT": return state - action.sub;
        case "RESET": return state = 0;
        


        default: return state
    }
}


export default CounterReducer;